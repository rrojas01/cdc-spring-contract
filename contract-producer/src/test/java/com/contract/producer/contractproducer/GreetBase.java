package com.contract.producer.contractproducer;

import io.restassured.module.mockmvc.RestAssuredMockMvc;
import org.junit.Before;

import java.util.HashMap;


public class GreetBase { // don't use Test as suffix otherwise Junit will try to run test here, ending up with a not runnable test exception
    private ConfigurationProperties configurationProperties;

    @Before
    public void setUp() {

        configurationProperties = new ConfigurationProperties();
        configurationProperties.setMap(new HashMap<String,String>() {{
            put("es","Hola");
        }});
        RestAssuredMockMvc.standaloneSetup(new GreetController(configurationProperties));
    }
}
