package com.contract.producer.contractproducer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;

@SpringBootApplication
@EnableConfigurationProperties(ConfigurationProperties.class)
public class ContractProducerApplication {

	public static void main(String[] args) {
		SpringApplication.run(ContractProducerApplication.class, args);
	}

}
