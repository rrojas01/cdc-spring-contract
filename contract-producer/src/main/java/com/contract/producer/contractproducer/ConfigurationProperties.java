package com.contract.producer.contractproducer;

import java.util.HashMap;
import java.util.Map;

@org.springframework.boot.context.properties.ConfigurationProperties(prefix = "locales")
public class ConfigurationProperties {

    private  Map<String,String> map = new HashMap<>();

    public Map<String,String> getMap() {
        return this.map;
    }

    public void setMap(Map<String,String> mockedMap) {
        this.map = mockedMap;
    }
}
