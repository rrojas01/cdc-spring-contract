package com.contract.producer.contractproducer;

import java.util.HashMap;
import java.util.Map;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1/greeting")
public class GreetController {

    private final ConfigurationProperties localesConfiguration;

    public GreetController(ConfigurationProperties localesConfiguration) {
        this.localesConfiguration = localesConfiguration;
    }

    @GetMapping("/findGreet")
    public ResponseEntity<Map<String,String>> findGreetByLocale(@RequestParam String localeId) {
        String localedGreet = "";
        if (localesConfiguration.getMap().containsKey(localeId)) {
            localedGreet = localesConfiguration.getMap().getOrDefault(localeId,"");
        }

        Map<String,String> data = new HashMap<>();
        data.put("greet",localedGreet);

        return ResponseEntity.ok(data);
    }
}
