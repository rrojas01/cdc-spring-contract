package com.contract.consumer.contractconsumer;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class GlobalConfiguration {

    @Bean
    public RestTemplate getRestTemplateInstance() {
        return new RestTemplate();
    }
}
