package com.contract.consumer.contractconsumer;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(name = "contract-producer-service")
public interface ProducerServiceClient {

    @GetMapping(value = "/v1/greeting/findGreet")
    GreetResponse getGreetByLocaleId(@RequestParam("localeId") String localeId);
}
