package com.contract.consumer.contractconsumer;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import java.util.HashMap;
import java.util.Map;

@RestController
@RequestMapping("/v1/greeting")
public class GreetController {

    private final RestTemplate restTemplate;
    private final ProducerServiceClient producerServiceClient;

    public GreetController(
            RestTemplate restTemplate,
            ProducerServiceClient producerServiceClient) {

        this.restTemplate = restTemplate;
        this.producerServiceClient = producerServiceClient;
    }

    @PostMapping("/{name}/locale/{localeId}")
    public ResponseEntity<Map<String,String>>multiLanguageGreet(@PathVariable String name,@PathVariable String localeId) {
        Map<String,String> queryParams = new HashMap<String,String>(){{
            put("localeId",localeId);
        }};

        GreetResponse response = producerServiceClient.getGreetByLocaleId(localeId);

        Map<String,String> responseBody = new HashMap<>();
        responseBody.put("greet",String.format("%s, %s", response.getGreet(), name));

        return ResponseEntity.ok(responseBody);
    }
}
