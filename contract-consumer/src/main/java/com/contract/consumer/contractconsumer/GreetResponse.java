package com.contract.consumer.contractconsumer;

import java.util.Map;

public class GreetResponse {
    private String greet;

    public String getGreet() {
        return greet;
    }

    public void setGreet(String greet) {
        this.greet = greet;
    }
}
