package com.contract.consumer.contractconsumer;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.cloud.contract.stubrunner.spring.AutoConfigureStubRunner;
import org.springframework.cloud.contract.stubrunner.spring.StubRunnerProperties;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@RunWith(SpringRunner.class)
@AutoConfigureStubRunner(
        ids = {"com.contract.producer:contract-producer:+:stubs:8082"},
        stubsMode = StubRunnerProperties.StubsMode.LOCAL)
public class GreetTest {

    @Autowired
    private WebApplicationContext context;

    private MockMvc mvc;
    private ObjectMapper mapper;

    @Before
    public void setup() {
        mapper = new ObjectMapper();
        mvc = MockMvcBuilders.webAppContextSetup(context).build();
    }

    @Test
    public void should_returnHola_calling_stubService() throws Exception {

        String greetResponse = "Hola, Rodolfo";

        mvc.perform(MockMvcRequestBuilders.post("/v1/greeting/Rodolfo/locale/es"))
                .andExpect(status().isOk())
                .andExpect((jsonPath("$.greet").value(greetResponse)));
    }


}
