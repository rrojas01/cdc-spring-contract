# CDC-Spring-Contracts

This project aims to show you how to setup and write integration test integrating Spring-Contracts, so we can have a full communication
accross the services involved in certain functionality.

For this example, we will have two small microservices ( contract-consumer and contract-producer) and a discovery-service, to register each service.
>
> - serviceId: contract-consumer
> - serviceId: contract-producer-service (this is intencional to have the serviceId different of the artifact-id in the POM)

###Doing this we can have a contract between the client consumer and service producer, this means:
>
> - very fast feedback 
> - not complext test env to have all the infrastructure need for the services to run
> - make the calls against stubs can prevent to have client working on local but failing on prod dute to differences on the contract.
> - we can get updates on the stub in any change occurred on the service producer inmediatelly.

## Spring Contract Setup
### Contract Producer 
First you need to add the needed dependencies and **Maven** plugin to enable the support of the contract creation, 
as well as generete the test used to test the created contract against the endpoint implementation.

*** Maven Setup ***

	<dependencies>
		 <dependency>
            <groupId>org.springframework.cloud</groupId>
            <artifactId>spring-cloud-starter-contract-verifier</artifactId>
            <scope>test</scope>
        </dependency>
	</dependencies>
	<build>
		<plugins>
          	<plugin>
                <groupId>org.springframework.cloud</groupId>
                <artifactId>spring-cloud-contract-maven-plugin</artifactId>
                <version>2.1.2.RELEASE</version>
                <extensions>true</extensions>
                <configuration>
                    <packageWithBaseClasses>com.contract.producer.contractproducer</packageWithBaseClasses> (1)
                </configuration>
            </plugin>
		</plugins>
	</build>

**(1)** used to lookup for the base test classes, each test class name needs to match with the directory where the contract in placed 
*+* plus the suffix *Base*, Spring Contract works by convention over configuration.

***Contract Setup Example***

**Contract Path:** src/test/resources/Greet

     description: Contract for get localed greet
     name: should find greet by localeId
     priority: 8
     request:
      url: /v1/greeting/findGreet
      method: GET
      queryParameters:
       localeId: es
      response:
       status: 200
       body:
        greet: Hola
       headers:
      content-type: application/json;charset=UTF-8

**Note:**  each .yml or groovy contract under a single folder, will be tranform into a test case when tests created.

***Base Test Example***
For this example we use RestAssuredMockMvc to test the controller endpoints.
>

    public class GreetBase { // don't use Test as suffix otherwise Spring will try to run test here, ending up with a not runnable test exception
      private ConfigurationProperties configurationProperties;

      @Before
      public void setUp() {
        configurationProperties = new ConfigurationProperties();
        configurationProperties.setMap(new HashMap<String,String>() {{
            put("es","Hola");
        }});
        RestAssuredMockMvc.standaloneSetup(new GreetController(configurationProperties));
      }
    }

To generete the test for the contract created run either of these commmand *./mvnw clean install* or *mvn clean install*.
**Note:** To follow the TDT we first create the contract and genrete the test so far we don't have the implementation craeted the test will failied.

Next test will be write the implementation. [ Enpoint implemetation ommited here to have a short doc here, please take a look at the source code ]

## Contract Consumer

*** Maven Setup ***

    <depencencies>
      <dependency>
	    <groupId>org.springframework.cloud</groupId>
	    <artifactId>spring-cloud-starter-contract-stub-runner</artifactId>
	    <scope>test</scope>
     </dependency>
    </dependencies>

***Test Setup Example***
**Note:** For the test we take leverage of stub runner configuration so it will take over of donwload/look up for the stub, once the contract is downloaded
it will be serve to the Wiremock server which in integrated with StubRunner.

Now the StubMode is set as local since this is not integration with any remote artifctory and as soon as the producer is deploying the artifact to an remote repo
we can switch to REMOTE and start ask for the latest sub and download it from the remote repository.

>

    @SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
    @RunWith(SpringRunner.class)
    @AutoConfigureStubRunner(
        ids = {"com.contract.producer:contract-producer:+:stubs:8082"},
        stubsMode = StubRunnerProperties.StubsMode.LOCAL)
    public class GreetTest {

     @Autowired
     private WebApplicationContext context;

     private MockMvc mvc;
     private ObjectMapper mapper;

     @Before
     public void setup() {
         mapper = new ObjectMapper();
         mvc = MockMvcBuilders.webAppContextSetup(context).build();
     }

     @Test
     public void should_returnHola_calling_stubService() throws Exception {

         String greetResponse = "Hola, Rodolfo";

         mvc.perform(MockMvcRequestBuilders.post("/v1/greeting/Rodolfo/locale/es"))
                 .andExpect(status().isOk())
                 .andExpect((jsonPath("$.greet").value(greetResponse)));
     }


    }



In order to have Feing working along with integration test, we need to map the artifact-id the stub-runner used and the serviceId used by feing.
To do this we take leverage of the subrunner configuration in the applicaton properties ( could be setup in the application-test.yml) if we had *'test' profile activeted. 
To keep the example simple, it was setup just in the application.yml.
>


    stubrunner:
     idsToServiceIds:
      contract-producer: contract-producer-service


